
public class BubbleSort extends Algoritimos{

	@Override
	public void Ordenar(int[] numeros) {
		Utilitaria util = new Utilitaria();
		
		int[] numerosDesordenados = numeros;
		long inicioCronometro = System.nanoTime();
			bubbleSort(numerosDesordenados);
		long fimCronometro = System.nanoTime();
		
		util.mostrarTempoUtilizado(inicioCronometro, fimCronometro,"BubbleSort",numeros);
	}
	
	private void bubbleSort(int[] numeros) {	
		for(int i = 0; i < numeros.length; i++){			
			for(int j = 0; j <numeros.length-1; j++){	
				if(numeros[j] > numeros[j+1]){				
					// Trocando os n�meros de posi��o
					int aux = numeros[j+1];
					numeros[j+1] = numeros[j];
					numeros[j] = aux;			
				}				
			}
		}	
	}	
}
