
public class InsertionSort extends Algoritimos{

	@Override
	public void Ordenar(int[] numeros) {
		Utilitaria util = new Utilitaria();
		// Clonando o Vetor Desordenado
		int[] numerosDesordenados = numeros;// Copiando os dados desordenados para um vetor da pr�pria classe
		long inicioCronometro = System.nanoTime();// Para que assim n�o ordene o vetor principal e
		insertionSort(numerosDesordenados);						//  interfira nos pr�ximos m�todos
		long fimCronometro = System.nanoTime();
		
		util.mostrarTempoUtilizado(inicioCronometro, fimCronometro,"InsertionSort",numeros);
	}

	private void insertionSort(int[] numeros) {
		for(int i = 0; i < numeros.length; i++){						
			int comparador = numeros[i];
			int j = i;				
			while(( j > 0) && (numeros[j-1] > comparador)){					
				numeros[j] = numeros[j-1];
				j--;					
			}
			numeros[j] = comparador;	
		}			
		
	}
}
