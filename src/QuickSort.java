
public class QuickSort extends Algoritimos{

	@Override
	public void Ordenar(int[] numeros) {		
		Utilitaria util = new Utilitaria();
		
		int[] numerosDesordenados = numeros;
		long inicioCronometro = System.nanoTime();//Iniciando Contagem de tempo utilizado no m�todo
		quickSort(numerosDesordenados, 0, numerosDesordenados.length-1);
		long fimCronometro = System.nanoTime();// Encerrando Contagem de tempo
		
		//Chamada do m�todo para mostrar na tela as informa��es coletadas
		util.mostrarTempoUtilizado(inicioCronometro, fimCronometro,"QuickSort",numeros);
	}

	public void quickSort(int[] numeros, int inicio, int fim){
		
		int esq = inicio;
		int dir = fim;
		int meio = numeros[(inicio+fim)/2];
		int troca;
		
		// Enquanto a esquerda for menor que a direita
		while(esq < dir){
			// Enquanto os n�meros da esquerda forem menor que o meio
			// E esquerda menor que direita
			while(numeros[esq] < meio && esq < dir){
				esq++;
			}
			while(numeros[dir] > meio && esq < dir){
				dir--;
			}
			// Se a esquerda ainda for menor que a direita
			if(esq <= dir){
				troca = numeros[esq];
				numeros[esq] = numeros[dir];
				numeros[dir] = troca;
				esq++;
				dir--;
			}
		}
		if(dir > inicio){
			quickSort(numeros, inicio, dir);
		}
		if(esq < fim){
			quickSort(numeros,esq,fim);
		}		
	}		
}
	


