
public class SelectionSort extends Algoritimos{

	@Override
	public void Ordenar(int[] numeros) {
		Utilitaria util = new Utilitaria();
		
		int[] numerosDesordenados = numeros;
		long inicioCronometro = System.nanoTime();
		selectionSort(numerosDesordenados);
		long fimCronometro = System.nanoTime();
		
		util.mostrarTempoUtilizado(inicioCronometro, fimCronometro,"SelectionSort", numeros);
}

	private void selectionSort(int[] numeros) {
		
		for(int i = 0; i < numeros.length; i++){
			int minimo = 0;
			for(int j = i+1; j< numeros.length; j++){
				// Caso tenha algum numeromero menor que ele,
				// � feita a troca com o m�nimonimo
				if(numeros[j] < numeros[minimo]){
					minimo = j;
				}
			}
			// Se o m�nimo for diferente do primeiro numero n�o ordenado
			// Ele faz a troca para orden�-los
			if(i != minimo){
				int aux = numeros[i];
				numeros[i] = numeros[minimo];
				numeros[minimo] = aux;
			}								
		}	
	}	
}	


