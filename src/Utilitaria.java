// Importando todos as classe e bibliotecas necessarias

import java.io.BufferedReader; // Importado pois cont�m o m�todo ReadLine que le linha a linha do arquivo
import java.io.File; // Necess�rio para que haja acesso a ENTRADA e SA�DA de arquivos
import java.io.FileReader; // Necess�rio para que haja LEITURA de Arquivos
import java.io.IOException; // Serve para Validar Excess�es de Entrada e Sa�da de Arquivos IO
import javax.swing.JOptionPane;

public class Utilitaria {

	public int[] gerarDadosAutomaticamente(int tamanho){
		
		int[] numeros = new int[tamanho];
		// Gerando Dados Aleat�rios
		for(int i = 0; i < tamanho; i++){
			int r = (int)(Math.random());
						
			if(r > 0){
				 numeros[i] = (int)(Math.random()*100) % r;
			}
			else{
				numeros[i] = (int)(Math.random()*100) % 100;
			}
		}		
		return numeros;	
	}
	
	public int[] informarDados(int tamanho){
		
		//@SuppressWarnings("resource")
		//Scanner entrada = new Scanner(System.in);
		int[] numeros = new int[tamanho];
		
		for(int i = 0; i < tamanho; i++){
			numeros[i] = Integer.parseInt(JOptionPane.showInputDialog("Digite o valor do " + (i+1) +"� N�mero: "));
		}

		return numeros;
	}
	
	int[] lerArquivo(){

		// Instancia da Classe FILE, para abrir arquivos
		File diretorio = new File("C:\\TutorialArquivo"); // Caminho do Arquivo
		File arquivo = new File(diretorio, "teste.txt"); // Nome Do Arquivo
		
		// Indicando o arquivo que ser� lido		
		FileReader fileReader = null;
		
		// Uso do Try Catch para que caso o Arquivo n�o seja encontrado
		// O programa pare de Executar 
		try{
			fileReader = new FileReader(arquivo);
		} catch (java.io.FileNotFoundException e) {
			// Apenas Visual, poderia ser uma mensagem na m�quina virtual
			// Por�m quisemos fazer uso dessa interface
			JOptionPane.showMessageDialog(null, "N�o foi poss�vel abrir o arquivo para leitura");
			e.printStackTrace();		
		}
		// Para a entrada de dados do Arquivo
		// Criamos o objeto bufferRead que nos
		// Oferece o m�todo de leitura readLine()
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		
		//String vai receber cada linha do Arquivo
		String linha = "";
		
		// Aqui � feito um looping linha a linha do arquivo
		// enquanto as linhas for�m diferente de null.
		// O m�todo readLine() devolve a linha na posi��o		
		// Do Loop para a variavel declarada a cima
		
		String[] partes = null;
		
		try{
			while ((linha = bufferedReader.readLine()) != null){
				// Aqui pegamos os dados da linha
				// "Quebramos" em partes para que caiba no vetor
				// Como s�o n�meros separador por espa�o � utilizado o Split			
				partes = linha.split(" ");
			}
		} catch (IOException e){
			JOptionPane.showMessageDialog(null,"N�o h� dados no arquivo !!!");
				e.printStackTrace();
			}
		// Vetor onde ficar� os n�meros do arquivo
		int[] numeros = new int[partes.length];
		// Passando os n�meros do arquivo para o vetor, 
		//� necess�rio converte-los de String para int
		for(int i = 0; i < numeros.length; i++){
			
			numeros[i] = Integer.parseInt(partes[i]);
		
		}
		// � necess�rio fechar o Arquivo
		try{
			fileReader.close();	// Esse Try Catch esta aqui para fechar o Arquivo	
		} catch (IOException e) {// Caso de erro, vai ser dado uma Exce��o do Tipo IO
			System.out.println("N�o foi poss�vel fechar o arquivo !!!");
		}				
		return numeros;		
	}
	
	public void mostrarTempoUtilizado(long tempoInicial, long tempoFinal, String nomeAlgoritmo, int[] numeros){		
		String tempoTotal = String.format("%.9f",(double)(tempoFinal - tempoInicial)/1000000000);
		
		JOptionPane.showMessageDialog(null, ("O Algoritmo " + nomeAlgoritmo + " Utilizou " + tempoTotal + " Segundos para Ordenar " + 
				numeros.length + " n�meros"));
				
	}

	public void IniciarOrdenacoes(int[] numeros) {
		Algoritimos algoritmos;
		// Chamando Cada Classe Com seus repectivos m�todos de Ordena��o	
		algoritmos = new BubbleSort();
		algoritmos.Ordenar(numeros);
		
		algoritmos = new InsertionSort();
		algoritmos.Ordenar(numeros);	
		
		algoritmos = new QuickSort();
		algoritmos.Ordenar(numeros);		
		
		algoritmos = new SelectionSort();
		algoritmos.Ordenar(numeros);
	}
}
